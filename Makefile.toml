[config]
default_to_workspace = false
skip_core_tasks = true

[tasks.fetch]
description = "Fetch dependencies"
script = '''
cargo fetch --locked
'''

[tasks.check-format]
description = "Check format of Rust source code with rustfmt"
dependencies = ["fetch"]
script = '''
cargo fmt --all -- --check
'''

[tasks.format]
description = "Reformat Rust source code with rustfmt"
dependencies = ["fetch"]
script = '''
cargo fmt --all --
'''

[tasks.lint]
description = "Check usage of Rust idioms with clippy"
dependencies = ["fetch"]
script = '''
cargo clippy --frozen --all-targets --all-features -- -D warnings
'''

[tasks.check]
description = "Check types of Rust source code"
dependencies = ["fetch"]
script = '''
cargo check --frozen --all-targets --all-features
'''

[tasks.build]
description = "Build crate"
dependencies = ["fetch"]
script = '''
cargo build --frozen --all-targets --all-features
'''

[tasks.build-tests]
description = "Build crate's tests"
dependencies = ["fetch"]
script = '''
cargo test --frozen --all-targets --all-features --no-run
'''

[tasks.test]
description = "Run crate's tests"
dependencies = ["build-tests"]
script = '''
cargo test --frozen --all-targets --all-features
'''

[tasks.doc]
description = "Build crate's documentation"
dependencies = ["fetch"]
script = '''
cargo doc --frozen --all-targets --all-features --no-deps
'''

[tasks.build-release]
description = "Build crate in release mode"
dependencies = ["fetch"]
script = '''
cargo build --frozen --all-features --release
'''

[tasks.run]
description = "Run crate"
dependencies = ["build"]
script = '''
cargo run --frozen --all-features
'''

[tasks.run-release]
description = "Run crate in release mode"
dependencies = ["build-release"]
script = '''
cargo run --frozen --all-features --release
'''

[tasks.clean-release]
description = "Clean release target directory"
script = '''
cargo clean --release --frozen
'''

[tasks.clean]
description = "Clean release target directory"
script = '''
cargo clean --frozen
'''

[tasks.audit]
description = "Perform audit on the crate"
script = '''
cargo audit --deny unsound --deny yanked
'''

[tasks.mutants]
description = "Run mutation testing tool"
script = '''
cargo mutants
'''

[tasks.default]
alias = "check"
