// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::process::Command;

use assert_cmd::{assert::OutputAssertExt, cargo::CommandCargoExt};

use predicates::str::contains;

use url::Url;

use serde_json::json;

use httpmock::Method::POST;
use httpmock::MockServer;

#[test]
fn success() -> Result<(), Box<dyn std::error::Error>> {
    let server = MockServer::start();

    let mock = server.mock(|when, then| {
        when.method(POST)
            .path("/message")
            .header("X-Gotify-Key", "123")
            .json_body(json!({
                "title": "some title",
                "message": "some message",
            }));
        then.status(200)
            .header("Content-Type", "application/json")
            .json_body(json!({
                "id": 1,
                "appid": 2,
                "date": "2020-12-21T22:53:23.123456789+01:00",
                "priority": 3,
                "title": "some title",
                "message": "some message",
            }));
    });

    let host = Url::parse(&server.base_url())?;

    let mut cmd = Command::cargo_bin("drone-gotify")?;
    cmd.arg("--host")
        .arg(host.as_str())
        .arg("--token")
        .arg("123")
        .arg("--title")
        .arg("some title")
        .arg("--message")
        .arg("some message");
    cmd.assert().success();

    mock.assert();

    Ok(())
}

#[test]
fn unauthorized() -> Result<(), Box<dyn std::error::Error>> {
    let server = MockServer::start();

    let mock = server.mock(|when, then| {
        when.method(POST)
            .path("/message")
            .header("X-Gotify-Key", "456");
        then.status(401)
            .header("Content-Type", "application/json")
            .json_body(json!({
                "error": "Unauthorized",
                "errorCode": 401,
                "errorDescription": "you need to provide a valid access token or user credentials to access this api",
            }));
    });

    let host = Url::parse(&server.base_url())?;

    let mut cmd = Command::cargo_bin("drone-gotify")?;
    cmd.arg("--host")
        .arg(host.as_str())
        .arg("--token")
        .arg("456")
        .arg("--title")
        .arg("some title")
        .arg("--message")
        .arg("some message");
    cmd.assert().failure().stderr(contains("Unauthorized"));

    mock.assert();

    Ok(())
}
