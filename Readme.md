Drone Gotify Plugin
====

A plugin to send messages to Gotify server from Drone pipelines

<https://gitlab.com/claudiomattera/drone-gotify/>


[Drone] is a solution for continuous integration pipelines, which supports [plugins][Drone plugin].
[Gotify] is a notification system.
This project allows to post notifications to a Gotify server from a Drone pipeline, e.g. when a step failed or when a pipeline completes.


```bob
  Drone
  Builds
  ╭───────╮            ╭──────────╮
 ╭┴──────╮│            │  Gotify  │
╭┴──────╮├╯───────────>│  Server  │
│       ├╯             │          │
╰───────╯              ╰──────────╯
                            │
      ┌──────────────┬──────┴─────────┐
      │              │                │
      V              V                V
╭──────────╮    ╭──────────╮    ╭──────────╮
│  Gotify  │    │  Gotify  │    │  Gotify  │
│  Client  │    │  Client  │    │  Client  │
╰──────────╯    ╰──────────╯    ╰──────────╯
```


Usage
----

### As a Drone plugin

This application can be directly used as a [Drone plugin] by adding the proper configuration to `.drone.yml`.
The required settings are Gotify host, token, title, and message.
The priority and log level can be set as an optional setting.

The following example enables notifications within a pipeline, reporting the individual failing steps.

~~~~yaml
---
kind: pipeline
type: docker
name: default

steps:
- name: first step
  image: alpine
  commands:
  - echo hi

- name: second step
  image: alpine
  commands:
  - false

- name: notification
  depends_on:
  - first step
  - second step
  when:
    status:
      - failure
  image: drone-gotify
  settings:
    host:
      from_secret: gotify_host
    token:
      from_secret: gotify_token
    title: >
        ✗ Build {{drone.build.number}} failed for {{drone.repo.name}}
    message: >
        Commit *{{drone.commit.message | trim}}* pushed by {{drone.commit.author.name}} on {{drone.commit.branch}}

        Failed steps:

        {% for step in drone.failed.steps | split(pat=",") %}
        - {{ step }}
        {% endfor %}

~~~~


The following example enables notifications over multiple pipelines, reporting the individual failing pipelines (stages).

~~~~yaml
---
kind: pipeline
type: docker
name: other stage
steps:
- name: first step
  image: alpine
  commands:
  - echo hi

- name: second step
  image: alpine
  commands:
  - false

---
kind: pipeline
type: docker
name: notification

depends_on:
- other stage

trigger:
  status:
  - failure

steps:
- name: notification
  image: drone-gotify
  settings:
    host:
      from_secret: gotify_host
    token:
      from_secret: gotify_token
    title: >
        ✗ Build {{drone.build.number}} failed for {{drone.repo.name}}
    message: >
        Commit *{{drone.commit.message | trim}}* pushed by {{drone.commit.author.name}} on {{drone.commit.branch}}

        Failed stages:

        {% for stage in drone.failed.stages | split(pat=",") %}
        - {{ stage }}
        {% endfor %}
~~~~


### From command line

The executable reads host, token, title, message and priority from command-line arguments or from environment variables.

~~~~plain
drone-gotify 1.3.0-dev
Claudio Mattera <dev@claudiomattera.it>
A plugin to send messages to Gotify server from Drone pipelines

USAGE:
    drone-gotify [OPTIONS] --host <HOST> --token <TOKEN> --title <TITLE> --message <MESSAGE>

OPTIONS:
    -h, --host <HOST>              Gotify host [env: PLUGIN_HOST=]
        --help                     Print help information
    -l, --log-level <LOG_LEVEL>    Log level [env: PLUGIN_LOG_LEVEL=2]
    -m, --message <MESSAGE>        Message body [env: PLUGIN_MESSAGE=]
    -p, --priority <PRIORITY>      Message priority [env: PLUGIN_PRIORITY=]
    -t, --title <TITLE>            Message title [env: PLUGIN_TITLE=]
        --token <TOKEN>            Gotify authentication token [env: PLUGIN_TOKEN=]
    -v, --verbose                  Increase verbosity (-v, -vv, -vvv, etc.)
    -V, --version                  Print version information
~~~~


### Installation from source

This application is implemented in Rust and can be compiled using the following command.

~~~~shell
cargo build --release
~~~~

The executable will be created in `target/release/drone-gotify`.


Message Templates
----

Title and message are rendered through [Tera] template engine.
All [Drone environment variables] are available in the variable `drone`, and all generic environment variables are available in the variable `env`.


### Available Drone Variables

In general, each variable in the form of `DRONE_SOME_VARIABLE` can be used in templates as `drone.some.variable`.
When a Drone environment variable is a prefix of other variables (e.g. `DRONE_COMMIT` and `DRONE_COMMIT_BRANCH`, or `DRONE_SEMVER` and `DRONE_SEMVER_MAJOR`), the former is available with a suffix `_complete`.

- [`drone.branch`](https://docs.drone.io/pipeline/environment/reference/drone-branch/)
- [`drone.build.action`](https://docs.drone.io/pipeline/environment/reference/drone-build-action/)
- [`drone.build.created`](https://docs.drone.io/pipeline/environment/reference/drone-build-created/)
- [`drone.build.event`](https://docs.drone.io/pipeline/environment/reference/drone-build-event/)
- [`drone.build.finished`](https://docs.drone.io/pipeline/environment/reference/drone-build-finished/)
- [`drone.build.number`](https://docs.drone.io/pipeline/environment/reference/drone-build-number/)
- [`drone.build.parent`](https://docs.drone.io/pipeline/environment/reference/drone-build-parent/)
- [`drone.build.started`](https://docs.drone.io/pipeline/environment/reference/drone-build-started/)
- [`drone.build.status`](https://docs.drone.io/pipeline/environment/reference/drone-build-status/)
- `drone.calver.major_complete`
- `drone.calver.major.minor`
- `drone.calver.micro`
- `drone.calver.minor`
- `drone.calver.modifier`
- `drone.calver.short`
- [`drone.calver_complete`](https://docs.drone.io/pipeline/environment/reference/drone-calver/)
- [`drone.commit.after`](https://docs.drone.io/pipeline/environment/reference/drone-commit-after/)
- [`drone.commit.author.avatar`](https://docs.drone.io/pipeline/environment/reference/drone-commit-author-avatar/)
- [`drone.commit.author.email`](https://docs.drone.io/pipeline/environment/reference/drone-commit-author-email/)
- [`drone.commit.author.name`](https://docs.drone.io/pipeline/environment/reference/drone-commit-author-name/)
- [`drone.commit.author_complete`](https://docs.drone.io/pipeline/environment/reference/drone-commit-author/)
- [`drone.commit.before`](https://docs.drone.io/pipeline/environment/reference/drone-commit-before/)
- [`drone.commit.branch`](https://docs.drone.io/pipeline/environment/reference/drone-commit-branch/)
- [`drone.commit.link`](https://docs.drone.io/pipeline/environment/reference/drone-commit-link/)
- [`drone.commit.message`](https://docs.drone.io/pipeline/environment/reference/drone-commit-message/)
- [`drone.commit.ref`](https://docs.drone.io/pipeline/environment/reference/drone-commit-ref/)
- [`drone.commit.sha`](https://docs.drone.io/pipeline/environment/reference/drone-commit-sha/)
- [`drone.commit_complete`](https://docs.drone.io/pipeline/environment/reference/drone-commit/)
- [`drone.deploy.to`](https://docs.drone.io/pipeline/environment/reference/drone-deploy-to/)
- [`drone.failed.stages`](https://docs.drone.io/pipeline/environment/reference/drone-failed-stages/)
- [`drone.failed.steps`](https://docs.drone.io/pipeline/environment/reference/drone-failed-steps/)
- [`drone.git.http.url`](https://docs.drone.io/pipeline/environment/reference/drone-git-http-url/)
- [`drone.git.ssh.url`](https://docs.drone.io/pipeline/environment/reference/drone-git-ssh-url/)
- [`drone.pull.request`](https://docs.drone.io/pipeline/environment/reference/drone-pull-request/)
- [`drone.remote.url`](https://docs.drone.io/pipeline/environment/reference/drone-remote-url/)
- [`drone.repo.branch`](https://docs.drone.io/pipeline/environment/reference/drone-repo-branch/)
- [`drone.repo.link`](https://docs.drone.io/pipeline/environment/reference/drone-repo-link/)
- [`drone.repo.name`](https://docs.drone.io/pipeline/environment/reference/drone-repo-name/)
- [`drone.repo.namespace`](https://docs.drone.io/pipeline/environment/reference/drone-repo-namespace/)
- [`drone.repo.owner`](https://docs.drone.io/pipeline/environment/reference/drone-repo-owner/)
- [`drone.repo.private`](https://docs.drone.io/pipeline/environment/reference/drone-repo-private/)
- [`drone.repo.scm`](https://docs.drone.io/pipeline/environment/reference/drone-repo-scm/)
- [`drone.repo.visibility`](https://docs.drone.io/pipeline/environment/reference/drone-repo-visibility/)
- [`drone.repo_complete`](https://docs.drone.io/pipeline/environment/reference/drone-repo/)
- `drone.semver.build`
- [`drone.semver.error`](https://docs.drone.io/pipeline/environment/reference/drone-semver-error/)
- `drone.semver.major`
- `drone.semver.minor`
- `drone.semver.patch`
- `drone.semver.prerelease`
- `drone.semver.short`
- [`drone.semver_complete`](https://docs.drone.io/pipeline/environment/reference/drone-semver/)
- [`drone.source.branch`](https://docs.drone.io/pipeline/environment/reference/drone-source-branch/)
- [`drone.stage.arch`](https://docs.drone.io/pipeline/environment/reference/drone-stage-arch/)
- [`drone.stage.depends.on`](https://docs.drone.io/pipeline/environment/reference/drone-stage-depends-on/)
- [`drone.stage.finished`](https://docs.drone.io/pipeline/environment/reference/drone-stage-finished/)
- [`drone.stage.kind`](https://docs.drone.io/pipeline/environment/reference/drone-stage-kind/)
- [`drone.stage.machine`](https://docs.drone.io/pipeline/environment/reference/drone-stage-machine/)
- [`drone.stage.name`](https://docs.drone.io/pipeline/environment/reference/drone-stage-name/)
- [`drone.stage.number`](https://docs.drone.io/pipeline/environment/reference/drone-stage-number/)
- [`drone.stage.os`](https://docs.drone.io/pipeline/environment/reference/drone-stage-os/)
- [`drone.stage.started`](https://docs.drone.io/pipeline/environment/reference/drone-stage-started/)
- [`drone.stage.status`](https://docs.drone.io/pipeline/environment/reference/drone-stage-status/)
- [`drone.stage.type`](https://docs.drone.io/pipeline/environment/reference/drone-stage-type/)
- [`drone.stage.variant`](https://docs.drone.io/pipeline/environment/reference/drone-stage-variant/)
- [`drone.step.name`](https://docs.drone.io/pipeline/environment/reference/drone-step-name/)
- [`drone.step.number`](https://docs.drone.io/pipeline/environment/reference/drone-step-number/)
- [`drone.system.host`](https://docs.drone.io/pipeline/environment/reference/drone-system-host/)
- [`drone.system.hostname`](https://docs.drone.io/pipeline/environment/reference/drone-system-hostname/)
- [`drone.system.proto`](https://docs.drone.io/pipeline/environment/reference/drone-system-proto/)
- [`drone.system.version`](https://docs.drone.io/pipeline/environment/reference/drone-system-version/)
- [`drone.tag`](https://docs.drone.io/pipeline/environment/reference/drone-tag/)
- [`drone.target.branch`](https://docs.drone.io/pipeline/environment/reference/drone-target-branch/)


License
----

Copyright Claudio Mattera 2022

You are free to copy, modify, and distribute this application with attribution under the terms of the [MPL 2.0 license]. See the [`License.txt`](./License.txt) file for details.


[Drone]: https://docs.drone.io/
[Gotify]: https://gotify.net/
[Drone plugin]: https://docs.drone.io/plugins/overview/
[MPL 2.0 license]: https://opensource.org/licenses/MPL-2.0
[Tera]: https://tera.netlify.app/
[Drone environment variables]: https://docs.drone.io/pipeline/environment/reference/
