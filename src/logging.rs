// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use env_logger::Builder as LoggerBuilder;
use env_logger::Env as LoggerEnv;

pub fn setup_logging(verbosity: u8, log_level: Option<u8>) {
    let verbosity = log_level.unwrap_or(verbosity);
    let default_log_filter = match verbosity {
        0 => "warn",
        1 => "info",
        2 => "info,drone_gotify=debug",
        3 => "info,drone_gotify=trace",
        4 => "debug,drone_gotify=trace",
        _ => "trace",
    };
    let filter = LoggerEnv::default().default_filter_or(default_log_filter);
    LoggerBuilder::from_env(filter)
        .format_timestamp(None)
        .init();
}
