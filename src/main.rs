// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use log::*;

use std::env;
use std::process::exit;

use clap::Parser;

use drone_gotify::{send_gotify_message, GotifyError};

mod commandline;
mod logging;

use crate::commandline::Arguments;
use crate::logging::setup_logging;

fn main() {
    let arguments = Arguments::parse();
    setup_logging(arguments.verbosity, arguments.log_level);

    exit(match inner_main(arguments) {
        Ok(()) => 0,
        Err(error) => {
            error!("{}", error.to_detailed_string());
            1
        }
    });
}

fn inner_main(arguments: Arguments) -> Result<(), GotifyError> {
    let environment = env::vars().collect();

    send_gotify_message(
        arguments.host,
        &arguments.token,
        &arguments.title,
        &arguments.message,
        arguments.priority,
        &environment,
    )
}
