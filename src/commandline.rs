// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use clap::{ArgAction, Parser};

use url::Url;

/// A basic example
#[derive(Parser, Debug)]
#[clap(about, author, version)]
pub struct Arguments {
    /// Increase verbosity (-v, -vv, -vvv, etc.)
    #[clap(short, long = "verbose", action(ArgAction::Count))]
    pub verbosity: u8,

    /// Gotify host
    #[clap(short, long, value_parser, env = "PLUGIN_HOST")]
    pub host: Url,

    /// Gotify authentication token
    #[clap(long, value_parser, env = "PLUGIN_TOKEN")]
    pub token: String,

    /// Message title
    #[clap(short, long, value_parser, env = "PLUGIN_TITLE")]
    pub title: String,

    /// Message body
    #[clap(short, long, value_parser, env = "PLUGIN_MESSAGE")]
    pub message: String,

    /// Message priority
    #[clap(short, long, value_parser, env = "PLUGIN_PRIORITY")]
    pub priority: Option<u32>,

    /// Log level
    #[clap(short, long, value_parser, env = "PLUGIN_LOG_LEVEL")]
    pub log_level: Option<u8>,
}
