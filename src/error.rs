// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::error::Error;
use std::fmt::Write as _;
use std::io::Error as IoError;

use thiserror::Error;

use reqwest::header::InvalidHeaderValue;
use reqwest::Error as ReqwestError;

use serde_json::Error as SerdeJsonError;

use tera::Error as TeraError;

use time::error::Parse as ParseTimeError;

use url::ParseError as ParseUrlError;

/// Error sending a message to a Gotify server
#[derive(Error, Debug)]
pub enum GotifyError {
    /// error while performing an IO operation
    #[error("error while performing an IO operation")]
    Io(#[from] IoError),

    /// error while performing an HTTP request
    #[error("error while performing an HTTP request")]
    Reqwest(#[from] ReqwestError),

    /// error while creating an HTTP header
    #[error("error while creating an HTTP header")]
    InvalidHeaderValue(#[from] InvalidHeaderValue),

    /// error while serializing/deserializing a JSON value
    #[error("error while serializing/deserializing a JSON value")]
    SerdeJson(#[from] SerdeJsonError),

    /// error while rendering template
    #[error("error while rendering template")]
    Tera(#[from] TeraError),

    /// error while parsing a datetime
    #[error("error while parsing a datetime")]
    ParseTime(#[from] ParseTimeError),

    /// error while parsing a URL
    #[error("error while parsing a URL")]
    ParseUrl(#[from] ParseUrlError),

    /// error while using Gotify API
    #[error("error {error} ({code}) while using Gotify API: {description}")]
    Api {
        /// Error message
        error: String,
        /// Error code
        code: u32,
        /// Error detailed description
        description: String,
    },
}

impl GotifyError {
    /// Write all nested errors to a string
    #[must_use]
    pub fn to_detailed_string(&self) -> String {
        let mut output = String::new();
        output.push_str(&self.to_string());

        if let Some(error) = self.source() {
            output.push_str("\n  Caused by");
            let messages = error_to_messages(error);
            for (i, message) in messages.iter().enumerate() {
                write!(output, "\n    {}: {}", i, message).unwrap();
            }
        }

        output
    }
}

fn error_to_messages(error: &dyn Error) -> Vec<String> {
    fn inner(error: &dyn Error, mut messages: Vec<String>) -> Vec<String> {
        messages.push(error.to_string());
        if let Some(error) = error.source() {
            inner(error, messages)
        } else {
            messages
        }
    }

    inner(error, Vec::new())
}

#[cfg(test)]
mod tests {
    use super::*;

    use assert2::check;

    #[test]
    fn api_error() {
        let error = GotifyError::Api {
            error: "some error".into(),
            code: 42,
            description: "some description".into(),
        };

        check!(
            error.to_detailed_string()
                == "error some error (42) while using Gotify API: some description",
        );
    }

    #[test]
    fn from_io_error() {
        let io_error = std::io::Error::new(std::io::ErrorKind::NotFound, "Not found");
        let error: GotifyError = io_error.into();

        check!(
            error.to_detailed_string()
                == "error while performing an IO operation\n  Caused by\n    0: Not found",
        );
    }

    #[test]
    fn from_multiple_errors() {
        let io_error = url::ParseError::InvalidPort;
        let io_error = std::io::Error::new(std::io::ErrorKind::Other, io_error);
        let error: GotifyError = io_error.into();

        check!(
            error.to_detailed_string() ==
            "error while performing an IO operation\n  Caused by\n    0: invalid port number",
        );
    }
}
