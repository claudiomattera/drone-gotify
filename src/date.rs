// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use time::error::Parse as ParseError;
use time::format_description::well_known::Rfc3339;
use time::OffsetDateTime;

use serde::{self, Deserialize, Deserializer};

pub fn deserialize<'de, D>(deserializer: D) -> Result<OffsetDateTime, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    parse_datetime(&s).map_err(serde::de::Error::custom)
}

fn parse_datetime(s: &str) -> Result<OffsetDateTime, ParseError> {
    OffsetDateTime::parse(s, &Rfc3339)
}

#[cfg(test)]
mod test {
    use super::*;

    use assert2::check;

    #[test]
    fn parse_utc_ending_with_z() -> Result<(), ParseError> {
        let input = "2021-05-07T05:24:51.987608776Z";
        let expected =
            OffsetDateTime::from_unix_timestamp_nanos(1_620_365_091_987_608_776).unwrap();
        let actual = parse_datetime(input)?;
        check!(actual == expected);
        Ok(())
    }

    #[test]
    fn parse_utc() -> Result<(), ParseError> {
        let input = "2021-05-07T05:24:51.987608776+00:00";
        let expected =
            OffsetDateTime::from_unix_timestamp_nanos(1_620_365_091_987_608_776).unwrap();
        let actual = parse_datetime(input)?;
        check!(actual == expected);
        Ok(())
    }

    #[test]
    fn parse_plus_two() -> Result<(), ParseError> {
        let input = "2021-05-07T05:24:51.987608776+02:00";
        let expected =
            OffsetDateTime::from_unix_timestamp_nanos(1_620_357_891_987_608_776).unwrap();
        let actual = parse_datetime(input)?;
        check!(actual == expected);
        Ok(())
    }
}
