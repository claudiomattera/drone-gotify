// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use time::OffsetDateTime;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ErrorResponse {
    pub error: String,
    pub error_code: u32,
    pub error_description: String,
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct SuccessResponse {
    pub id: u64,
    pub appid: u64,

    #[serde(with = "crate::date")]
    pub date: OffsetDateTime,

    #[serde(skip_deserializing)]
    pub extras: Option<()>,

    pub title: String,
    pub message: String,
    pub priority: u64,
}

#[cfg(test)]
mod test {
    use super::*;

    use time::format_description::well_known::Rfc3339;

    use serde_json::from_str as from_json_str;
    use serde_json::json;
    use serde_json::to_string_pretty as to_json_string;

    use assert2::check;

    use crate::error::GotifyError;

    #[test]
    fn deserialize_response() -> Result<(), GotifyError> {
        let raw = json!({
            "id": 1,
            "appid": 2,
            "date": "2020-12-21T22:53:23.123456789+01:00",
            "priority": 3,
            "title": "Some title",
            "message": "Some message",
        });
        let text = to_json_string(&raw)?;
        let response: SuccessResponse = from_json_str(&text)?;

        let expected = SuccessResponse {
            id: 1,
            appid: 2,
            date: OffsetDateTime::parse("2020-12-21T22:53:23.123456789+01:00", &Rfc3339)?,
            extras: None,
            title: "Some title".into(),
            message: "Some message".into(),
            priority: 3,
        };

        check!(response == expected);

        Ok(())
    }
}
