// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::path::Path;

use log::{debug, info, trace};

use url::Url;

use reqwest::blocking::Client as ReqwestClient;
use reqwest::header;
use reqwest::Certificate;

use crate::error::GotifyError;

use super::{ErrorResponse, Message, SuccessResponse};

#[derive(Debug)]
pub struct Client {
    url: Url,
    http_client: ReqwestClient,
}

impl Client {
    pub fn new(mut url: Url, token: &str) -> Result<Self, GotifyError> {
        url.set_path("/message");

        let http_client = Self::build_http_client(token)?;

        Ok(Self { url, http_client })
    }

    fn build_http_client(token: &str) -> Result<ReqwestClient, GotifyError> {
        let mut headers = header::HeaderMap::new();
        headers.insert(
            header::ACCEPT,
            header::HeaderValue::from_static("application/json"),
        );

        let mut token_header = header::HeaderValue::from_str(token)?;
        token_header.set_sensitive(true);
        headers.insert("X-Gotify-Key", token_header);

        let mut http_client_builder = ReqwestClient::builder().default_headers(headers);

        let system_ca_path = Path::new("/etc/ssl/certs/ca-certificates.crt");
        if system_ca_path.exists() {
            debug!(
                "Adding system certificate authority {}",
                system_ca_path.display()
            );
            let certificate_data = std::fs::read(system_ca_path)?;
            let certificate = Certificate::from_pem(&certificate_data)?;
            http_client_builder = http_client_builder.add_root_certificate(certificate);
        }

        let http_client = http_client_builder.build()?;

        Ok(http_client)
    }

    pub fn send_message(&mut self, message: &Message) -> Result<(), GotifyError> {
        debug!("Sending message to {}", self.url);

        let request = self
            .http_client
            .post(self.url.clone())
            .json(message)
            .build()?;

        trace!("Request: {:?}", request);

        let response = self.http_client.execute(request)?;

        match response.error_for_status_ref() {
            Ok(_) => {
                let response = response.json::<SuccessResponse>()?;
                info!(
                    "Message posted correctly with id {} on {}",
                    response.id, response.date
                );
                Ok(())
            }
            Err(_error) => {
                let response = response.json::<ErrorResponse>()?;
                trace!("Response: {:?}", response);
                Err(GotifyError::Api {
                    error: response.error,
                    code: response.error_code,
                    description: response.error_description,
                })
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use serde_json::json;

    use url::Url;

    use httpmock::Method::POST;
    use httpmock::MockServer;

    use crate::MessageBuilder;

    #[test]
    fn correct_request() -> Result<(), GotifyError> {
        let server = MockServer::start();

        let message_mock = server.mock(|when, then| {
            when.method(POST)
                .path("/message")
                .header("X-Gotify-Key", "123");
            then.status(200)
                .header("Content-Type", "application/json")
                .json_body(json!({
                    "id": 1,
                    "appid": 2,
                    "date": "2020-12-21T22:53:23.123456789+01:00",
                    "priority": 1,
                    "title": "",
                    "message": "",
                }));
        });

        let host = Url::parse(&server.base_url())?;
        let mut client = Client::new(host, "123")?;

        let message = MessageBuilder::from_title("title")
            .with_body("message")
            .build();

        client.send_message(&message)?;

        message_mock.assert();

        Ok(())
    }

    #[test]
    #[should_panic(
        expected = "you need to provide a valid access token or user credentials to access this api"
    )]
    fn unauthorized_request() {
        let server = httpmock::MockServer::start();

        let message_mock = server.mock(|when, then| {
            when.method(POST)
                .path("/message")
                .header("X-Gotify-Key", "456");
            then.status(401)
                .header("Content-Type", "application/json")
                .json_body(json!({
                    "error": "Unauthorized",
                    "errorCode": 401,
                    "errorDescription": "you need to provide a valid access token or user credentials to access this api"
                }));
        });

        let host = Url::parse(&server.base_url()).unwrap();
        let mut client = Client::new(host, "456").unwrap();

        let message = MessageBuilder::from_title("title")
            .with_body("message")
            .build();

        client.send_message(&message).unwrap();

        message_mock.assert();
    }
}
