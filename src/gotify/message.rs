// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Message {
    title: String,
    message: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    priority: Option<u32>,
}

impl Message {
    #[allow(unused)]
    pub fn title(&self) -> &str {
        &self.title
    }

    #[allow(unused)]
    pub fn message(&self) -> &str {
        &self.message
    }

    #[allow(unused)]
    pub fn priority(&self) -> &Option<u32> {
        &self.priority
    }
}

#[derive(Debug)]
pub struct MessageBuilder {
    message: Message,
}

impl MessageBuilder {
    pub fn from_title(title: impl Into<String>) -> Self {
        Self {
            message: Message {
                title: title.into(),
                message: "".into(),
                priority: None,
            },
        }
    }

    pub fn with_body(mut self, body: impl Into<String>) -> Self {
        self.message.message = body.into();
        self
    }

    #[allow(unused)]
    pub fn with_priority(mut self, priority: u32) -> Self {
        self.message.priority = Some(priority);
        self
    }

    pub fn with_opt_priority(mut self, priority: Option<u32>) -> Self {
        self.message.priority = priority;
        self
    }

    #[allow(unused)]
    pub fn without_priority(mut self) -> Self {
        self.message.priority = None;
        self
    }

    pub fn build(self) -> Message {
        self.message
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_json::to_string as to_json_string;
    use serde_json::Error as SerdeJsonError;

    use assert2::check;

    #[test]
    fn test_to_json_without_priority() -> Result<(), SerdeJsonError> {
        let message = Message {
            title: "some title".into(),
            message: "some message".into(),
            priority: None,
        };

        let text = to_json_string(&message)?;

        let expected = r#"{"title":"some title","message":"some message"}"#;

        check!(text == expected);

        Ok(())
    }

    #[test]
    fn test_to_json_with_priority() -> Result<(), SerdeJsonError> {
        let message = Message {
            title: "some title".into(),
            message: "some message".into(),
            priority: Some(4),
        };

        let text = to_json_string(&message)?;

        let expected = r#"{"title":"some title","message":"some message","priority":4}"#;

        check!(text == expected);

        Ok(())
    }

    #[test]
    fn test_message_fields_with_priority() {
        let message = MessageBuilder::from_title("some title")
            .with_body("some message")
            .with_priority(4)
            .build();

        check!(message.title() == "some title");
        check!(message.message() == "some message");
        check!(message.priority() == &Some(4));
    }

    #[test]
    fn test_message_fields_with_opt_priority() {
        let message = MessageBuilder::from_title("some title")
            .with_body("some message")
            .with_opt_priority(Some(4))
            .build();

        check!(message.title() == "some title");
        check!(message.message() == "some message");
        check!(message.priority() == &Some(4));
    }

    #[test]
    fn test_message_fields_without_priority() {
        let message = MessageBuilder::from_title("some title")
            .with_body("some message")
            .with_priority(4)
            .without_priority()
            .build();

        check!(message.title() == "some title");
        check!(message.message() == "some message");
        check!(message.priority() == &None);
    }
}
