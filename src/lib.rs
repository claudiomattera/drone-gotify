// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Functions and data types for sending messages to a Gotify server

#![deny(missing_docs, clippy::pedantic)]
#![allow(clippy::implicit_hasher, clippy::module_name_repetitions)]

use std::collections::HashMap;

use log::trace;

use url::Url;

mod date;
mod error;
mod gotify;
mod template;

pub use crate::error::GotifyError;

use crate::gotify::{Client, MessageBuilder};

/// Send a message to a Gotify server
///
/// # Arguments
///
/// * `host` - Gotify server base URL
/// * `token` - Gotify authentication token
/// * `title` - Message title
/// * `message` - Message body
/// * `priority` - Message priority
/// * `environment` - Environment for template variables
///
/// # Errors
///
/// Will return a `GotifyError` in case the message could not be sent.
///
/// # Examples
///
/// ```no_run
/// # use std::collections::HashMap;
/// use drone_gotify::send_gotify_message;
/// # use drone_gotify::GotifyError;
/// # use url::Url;
///
/// send_gotify_message(
///     Url::parse("https://gotify.example.com/")?,
///     "123",
///     "some title",
///     "some message",
///     Some(3),
///     &HashMap::default(),
/// )?;
/// # Ok::<(), GotifyError>(())
/// ```
pub fn send_gotify_message(
    host: Url,
    token: &str,
    title: &str,
    message: &str,
    priority: Option<u32>,
    environment: &HashMap<String, String>,
) -> Result<(), GotifyError> {
    let title = template::render_template(title, environment)?;
    let message = template::render_template(message, environment)?;

    trace!("Rendered title: {}", title);
    trace!("Rendered message: {}", message);

    let mut client = Client::new(host, token)?;

    let message = MessageBuilder::from_title(title)
        .with_body(message)
        .with_opt_priority(priority)
        .build();

    client.send_message(&message)?;

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    use url::Url;

    use serde_json::json;

    use httpmock::Method::POST;
    use httpmock::MockServer;

    #[test]
    fn test_send_message() -> Result<(), GotifyError> {
        let server = MockServer::start();

        let message_mock = server.mock(|when, then| {
            when.method(POST)
                .path("/message")
                .header("X-Gotify-Key", "123");
            then.status(200)
                .header("Content-Type", "application/json")
                .json_body(json!({
                    "id": 1,
                    "appid": 2,
                    "date": "2020-12-21T22:53:23.123456789+01:00",
                    "priority": 3,
                    "title": "some title",
                    "message": "some message",
                }));
        });

        let host = Url::parse(&server.base_url())?;
        send_gotify_message(
            host,
            "123",
            "some title",
            "some message",
            Some(3),
            &HashMap::default(),
        )?;

        message_mock.assert();

        Ok(())
    }
}
