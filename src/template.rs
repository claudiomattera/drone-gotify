// Copyright Claudio Mattera 2022.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::collections::HashMap;

use serde_json::Map as JsonMap;
use serde_json::Value as JsonValue;

use tera::{Context, Tera};

use crate::error::GotifyError;

pub fn create_context_from_environment(
    environment: &HashMap<String, String>,
) -> Result<Context, GotifyError> {
    let environment_json = create_environment_json_mappings(environment);
    let drone_json = create_drone_json_mappings(environment);
    let mut context = Context::new();
    context.try_insert("drone", &drone_json)?;
    context.try_insert("env", &environment_json)?;
    Ok(context)
}

pub fn create_environment_json_mappings(environment: &HashMap<String, String>) -> JsonValue {
    let mut mapping = JsonMap::new();

    for (key, value) in environment.iter() {
        mapping.insert(key.clone(), JsonValue::String(value.clone()));
    }

    JsonValue::Object(mapping)
}

pub fn create_drone_json_mappings(environment: &HashMap<String, String>) -> JsonValue {
    let mut mapping = JsonMap::new();

    let mut sorted_environment: Vec<(String, String)> = environment
        .iter()
        .map(|(key, value)| (key.clone(), value.clone()))
        .collect();
    sorted_environment.sort_by_key(|pair| pair.0.clone());
    sorted_environment.reverse();

    for (key, value) in &sorted_environment {
        if let Some(stripped_key) = key.strip_prefix("DRONE_") {
            let mut elements: Vec<_> = stripped_key.split('_').map(str::to_lowercase).collect();
            let leaf = elements.pop().expect("");
            let mut container = &mut mapping;
            for branch in elements {
                if !container.contains_key(&branch) {
                    container.insert(branch.clone(), JsonValue::Object(JsonMap::new()));
                }
                container = container
                    .get_mut(&branch)
                    .expect("Impossible")
                    .as_object_mut()
                    .expect("Impossible");
            }
            let leaf = if container.contains_key(&leaf) {
                format!("{}_complete", leaf)
            } else {
                leaf
            };
            container.insert(leaf, JsonValue::String(value.clone()));
        }
    }

    JsonValue::Object(mapping)
}

pub fn render_template(
    template: &str,
    environment: &HashMap<String, String>,
) -> Result<String, GotifyError> {
    let context = create_context_from_environment(environment)?;
    let result = Tera::one_off(template, &context, false)?;
    Ok(result)
}

#[cfg(test)]
mod test {
    use super::*;

    use std::collections::HashMap;

    use serde_json::json;

    use lazy_static::lazy_static;

    use assert2::check;

    #[test]
    #[allow(clippy::too_many_lines)]
    fn test_create_drone_json_mappings() {
        let expected = json!(
        {
          "branch": "master",
          "build": {
            "action": "open",
            "created": "1609152054",
            "event": "push",
            "finished": "1609152071",
            "number": "72",
            "parent": "15",
            "started": "1609152061",
            "status": "success"
          },
          "calver": {
            "major": {
              "minor": "19.1"
            },
            "major_complete": "19",
            "micro": "0",
            "minor": "1",
            "modifier": "beta.20190318",
            "short": "19.1.0"
          },
          "calver_complete": "19.1.0-beta.20190318",
          "commit": {
            "after": "bcdd4bf0245c82c060407b3b24b9b87301d15ac1",
            "author": {
              "avatar": "https://githubusercontent.com/u/...",
              "email": "johndoe@example.com",
              "name": "John Doe"
            },
            "author_complete": "johndoe",
            "before": "bcdd4bf0245c82c060407b3b24b9b87301d15ac1",
            "branch": "master",
            "link": "http://www.example.com/johndoe/some_repo/pull/42",
            "message": "Updated README.md",
            "ref": "refs/heads/master",
            "sha": "bcdd4bf0245c82c060407b3b24b9b87301d15ac1"
          },
          "commit_complete": "bcdd4bf0245c82c060407b3b24b9b87301d15ac1",
          "deploy": {
            "to": "production"
          },
          "failed": {
            "stages": "build,test",
            "steps": "backend,frontend"
          },
          "git": {
            "http": {
              "url": "https://github.com/johndoe/some_repo.git"
            },
            "ssh": {
              "url": "git@github.com:johndoe/some_repo.git"
            }
          },
          "pull": {
            "request": "42"
          },
          "remote": {
            "url": "https://github.com/johndoe/some_repo.git"
          },
          "repo": {
            "branch": "master",
            "link": "https://github.com/johndoe/some_repo",
            "name": "some_repo",
            "namespace": "johndoe",
            "owner": "johndoe",
            "private": "false",
            "scm": "git",
            "visibility": "public"
          },
          "repo_complete": "johndoe/some_repo",
          "semver": {
            "build": "",
            "error": "",
            "major": "1",
            "minor": "2",
            "patch": "3",
            "prerelease": "alpha.1",
            "short": "1.2.3"
          },
          "semver_complete": "1.2.3-alpha.1",
          "source": {
            "branch": "feature/develop"
          },
          "stage": {
            "arch": "arm",
            "depends": {
              "on": "backend,frontend"
            },
            "finished": "1609152423",
            "kind": "pipeline",
            "machine": "hostname.localdomain",
            "name": "build",
            "number": "1",
            "os": "linux",
            "started": "1609152414",
            "status": "success",
            "type": "docker",
            "variant": "v7"
          },
          "step": {
            "name": "build",
            "number": "1"
          },
          "system": {
            "host": "drone.example.com",
            "hostname": "drone.example.com",
            "proto": "https",
            "version": "1.2.3"
          },
          "tag": "v1.0.0",
          "target": {
            "branch": "master"
          }
        }
                );
        let actual = create_drone_json_mappings(&ENVIRONMENT_VARIABLES);
        check!(actual == expected);
    }

    #[test]
    fn trivial() -> Result<(), GotifyError> {
        let template = "hello";
        let result = render_template(template, &ENVIRONMENT_VARIABLES)?;
        check!(result == "hello");
        Ok(())
    }

    #[test]
    fn replacement() -> Result<(), GotifyError> {
        let template = "hello {{ drone.repo.owner }}";
        let result = render_template(template, &ENVIRONMENT_VARIABLES)?;
        check!(result == "hello johndoe");
        Ok(())
    }

    #[test]
    fn environment_variable() -> Result<(), GotifyError> {
        let template = "hello {{ env.DRONE_REPO_OWNER }}, using Drone? {{ env.DRONE }}";
        let result = render_template(template, &ENVIRONMENT_VARIABLES)?;
        check!(result == "hello johndoe, using Drone? true");
        Ok(())
    }

    #[test]
    fn if_then_else() -> Result<(), GotifyError> {
        let template = "{% if drone.build.status == \"success\" %}
succeeded for build {{drone.build.number}}
{% else %}
failed for build {{drone.build.number}}
{% endif %}";
        let result = render_template(template, &ENVIRONMENT_VARIABLES)?;
        check!(
            result
                == "
succeeded for build 72
"
        );
        Ok(())
    }

    #[test]
    fn for_split() -> Result<(), GotifyError> {
        let template = "Failed steps:

{% for step in drone.failed.steps | split(pat=\",\") -%}
- {{ step }}
{% endfor -%}";
        let result = render_template(template, &ENVIRONMENT_VARIABLES)?;
        check!(
            result
                == "Failed steps:

- backend
- frontend
"
        );
        Ok(())
    }

    #[test]
    fn variable() -> Result<(), GotifyError> {
        let template = "{% set url = drone.system.proto ~ \"://\" ~ drone.system.host ~ \"/\" ~ drone.repo.owner ~ \"/\" ~ drone.repo.name ~ \"/\" ~ drone.build.number -%}
See complete build log at [{{url}}]({{url}})";
        let result = render_template(template, &ENVIRONMENT_VARIABLES)?;
        check!(result == "See complete build log at [https://drone.example.com/johndoe/some_repo/72](https://drone.example.com/johndoe/some_repo/72)");
        Ok(())
    }

    lazy_static! {
        static ref ENVIRONMENT_VARIABLES: HashMap<String, String> = {
            let mut variables = HashMap::new();
            variables.insert("DRONE".to_owned(), "true".to_owned());
            variables.insert("DRONE_BRANCH".to_owned(), "master".to_owned());
            variables.insert("DRONE_BUILD_ACTION".to_owned(), "open".to_owned());
            variables.insert("DRONE_BUILD_CREATED".to_owned(), "1609152054".to_owned());
            variables.insert("DRONE_BUILD_EVENT".to_owned(), "push".to_owned());
            variables.insert("DRONE_BUILD_FINISHED".to_owned(), "1609152071".to_owned());
            variables.insert("DRONE_BUILD_NUMBER".to_owned(), "72".to_owned());
            variables.insert("DRONE_BUILD_PARENT".to_owned(), "15".to_owned());
            variables.insert("DRONE_BUILD_STARTED".to_owned(), "1609152061".to_owned());
            variables.insert("DRONE_BUILD_STATUS".to_owned(), "success".to_owned());
            variables.insert("DRONE_CALVER".to_owned(), "19.1.0-beta.20190318".to_owned());
            variables.insert("DRONE_CALVER_SHORT".to_owned(), "19.1.0".to_owned());
            variables.insert("DRONE_CALVER_MAJOR_MINOR".to_owned(), "19.1".to_owned());
            variables.insert("DRONE_CALVER_MAJOR".to_owned(), "19".to_owned());
            variables.insert("DRONE_CALVER_MINOR".to_owned(), "1".to_owned());
            variables.insert("DRONE_CALVER_MICRO".to_owned(), "0".to_owned());
            variables.insert(
                "DRONE_CALVER_MODIFIER".to_owned(),
                "beta.20190318".to_owned(),
            );
            variables.insert(
                "DRONE_COMMIT".to_owned(),
                "bcdd4bf0245c82c060407b3b24b9b87301d15ac1".to_owned(),
            );
            variables.insert(
                "DRONE_COMMIT_AFTER".to_owned(),
                "bcdd4bf0245c82c060407b3b24b9b87301d15ac1".to_owned(),
            );
            variables.insert("DRONE_COMMIT_AUTHOR".to_owned(), "johndoe".to_owned());
            variables.insert(
                "DRONE_COMMIT_AUTHOR_AVATAR".to_owned(),
                "https://githubusercontent.com/u/...".to_owned(),
            );
            variables.insert(
                "DRONE_COMMIT_AUTHOR_EMAIL".to_owned(),
                "johndoe@example.com".to_owned(),
            );
            variables.insert("DRONE_COMMIT_AUTHOR_NAME".to_owned(), "John Doe".to_owned());
            variables.insert(
                "DRONE_COMMIT_BEFORE".to_owned(),
                "bcdd4bf0245c82c060407b3b24b9b87301d15ac1".to_owned(),
            );
            variables.insert("DRONE_COMMIT_BRANCH".to_owned(), "master".to_owned());
            variables.insert(
                "DRONE_COMMIT_LINK".to_owned(),
                "http://www.example.com/johndoe/some_repo/pull/42".to_owned(),
            );
            variables.insert(
                "DRONE_COMMIT_MESSAGE".to_owned(),
                "Updated README.md".to_owned(),
            );
            variables.insert(
                "DRONE_COMMIT_REF".to_owned(),
                "refs/heads/master".to_owned(),
            );
            variables.insert(
                "DRONE_COMMIT_SHA".to_owned(),
                "bcdd4bf0245c82c060407b3b24b9b87301d15ac1".to_owned(),
            );
            variables.insert("DRONE_DEPLOY_TO".to_owned(), "production".to_owned());
            variables.insert("DRONE_FAILED_STAGES".to_owned(), "build,test".to_owned());
            variables.insert(
                "DRONE_FAILED_STEPS".to_owned(),
                "backend,frontend".to_owned(),
            );
            variables.insert(
                "DRONE_GIT_HTTP_URL".to_owned(),
                "https://github.com/johndoe/some_repo.git".to_owned(),
            );
            variables.insert(
                "DRONE_GIT_SSH_URL".to_owned(),
                "git@github.com:johndoe/some_repo.git".to_owned(),
            );
            variables.insert("DRONE_PULL_REQUEST".to_owned(), "42".to_owned());
            variables.insert(
                "DRONE_REMOTE_URL".to_owned(),
                "https://github.com/johndoe/some_repo.git".to_owned(),
            );
            variables.insert("DRONE_REPO".to_owned(), "johndoe/some_repo".to_owned());
            variables.insert("DRONE_REPO_BRANCH".to_owned(), "master".to_owned());
            variables.insert(
                "DRONE_REPO_LINK".to_owned(),
                "https://github.com/johndoe/some_repo".to_owned(),
            );
            variables.insert("DRONE_REPO_NAME".to_owned(), "some_repo".to_owned());
            variables.insert("DRONE_REPO_NAMESPACE".to_owned(), "johndoe".to_owned());
            variables.insert("DRONE_REPO_OWNER".to_owned(), "johndoe".to_owned());
            variables.insert("DRONE_REPO_PRIVATE".to_owned(), "false".to_owned());
            variables.insert("DRONE_REPO_SCM".to_owned(), "git".to_owned());
            variables.insert("DRONE_REPO_VISIBILITY".to_owned(), "public".to_owned());
            variables.insert("DRONE_SEMVER".to_owned(), "1.2.3-alpha.1".to_owned());
            variables.insert("DRONE_SEMVER_BUILD".to_owned(), "".to_owned());
            variables.insert("DRONE_SEMVER_ERROR".to_owned(), "".to_owned());
            variables.insert("DRONE_SEMVER_MAJOR".to_owned(), "1".to_owned());
            variables.insert("DRONE_SEMVER_MINOR".to_owned(), "2".to_owned());
            variables.insert("DRONE_SEMVER_PATCH".to_owned(), "3".to_owned());
            variables.insert("DRONE_SEMVER_PRERELEASE".to_owned(), "alpha.1".to_owned());
            variables.insert("DRONE_SEMVER_SHORT".to_owned(), "1.2.3".to_owned());
            variables.insert(
                "DRONE_SOURCE_BRANCH".to_owned(),
                "feature/develop".to_owned(),
            );
            variables.insert("DRONE_STAGE_ARCH".to_owned(), "arm".to_owned());
            variables.insert(
                "DRONE_STAGE_DEPENDS_ON".to_owned(),
                "backend,frontend".to_owned(),
            );
            variables.insert("DRONE_STAGE_FINISHED".to_owned(), "1609152423".to_owned());
            variables.insert("DRONE_STAGE_KIND".to_owned(), "pipeline".to_owned());
            variables.insert(
                "DRONE_STAGE_MACHINE".to_owned(),
                "hostname.localdomain".to_owned(),
            );
            variables.insert("DRONE_STAGE_NAME".to_owned(), "build".to_owned());
            variables.insert("DRONE_STAGE_NUMBER".to_owned(), "1".to_owned());
            variables.insert("DRONE_STAGE_OS".to_owned(), "linux".to_owned());
            variables.insert("DRONE_STAGE_STARTED".to_owned(), "1609152414".to_owned());
            variables.insert("DRONE_STAGE_STATUS".to_owned(), "success".to_owned());
            variables.insert("DRONE_STAGE_TYPE".to_owned(), "docker".to_owned());
            variables.insert("DRONE_STAGE_VARIANT".to_owned(), "v7".to_owned());
            variables.insert("DRONE_STEP_NAME".to_owned(), "build".to_owned());
            variables.insert("DRONE_STEP_NUMBER".to_owned(), "1".to_owned());
            variables.insert(
                "DRONE_SYSTEM_HOST".to_owned(),
                "drone.example.com".to_owned(),
            );
            variables.insert(
                "DRONE_SYSTEM_HOSTNAME".to_owned(),
                "drone.example.com".to_owned(),
            );
            variables.insert("DRONE_SYSTEM_PROTO".to_owned(), "https".to_owned());
            variables.insert("DRONE_SYSTEM_VERSION".to_owned(), "1.2.3".to_owned());
            variables.insert("DRONE_TAG".to_owned(), "v1.0.0".to_owned());
            variables.insert("DRONE_TARGET_BRANCH".to_owned(), "master".to_owned());
            variables
        };
    }
}
