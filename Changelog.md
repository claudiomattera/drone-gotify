# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.3.2] - 2022-08-13

### Added

- Add tests
- Add Docker images for ARM and ARM64

### Changed

- Update dependencies
- Refactor to more idiomatic code
- Use [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) format for changelog


## [1.3.1] - 2022-07-04

### Changed

- Update dependencies
- Update Docker registry credentials in pipeline


## [1.3.0] - 2022-06-25

### Added

- Add setting `log_level` to set log level when used as Drone plugin

### Changed

- Replace crate `chrono` with `time`
- Replace crate `clap` with `structopt`
- Create Gotify API module
- Pass Gotify token as HTTP header instead of as query parameter
- Use Docker `scratch` image instead of Alpine


## [1.2.0] - 2021-12-18

### Changed

- Update dependencies
- Rework Dockerfile to use Alpine and optimize for size
- Change license from MIT to MPL-2.0


## [1.1.1] - 2021-05-07

### Changed

- Fix datetime parsing in response


## [1.1.0] - 2021-05-06

### Removed

- Remove ARM support


## [1.0.0] - 2020-12-31

### Added

- Initial implementation

[Unreleased]: https://git.claudiomattera.it/claudiomattera/drone-gotify
[1.3.2]: https://git.claudiomattera.it/claudiomattera/drone-gotify/releases/tag/1.3.2
[1.3.1]: https://git.claudiomattera.it/claudiomattera/drone-gotify/releases/tag/1.3.1
[1.3.0]: https://git.claudiomattera.it/claudiomattera/drone-gotify/releases/tag/1.3.0
[1.2.0]: https://git.claudiomattera.it/claudiomattera/drone-gotify/releases/tag/1.2.0
[1.1.1]: https://git.claudiomattera.it/claudiomattera/drone-gotify/releases/tag/1.1.1
[1.1.0]: https://git.claudiomattera.it/claudiomattera/drone-gotify/releases/tag/1.1.0
[1.0.0]: https://git.claudiomattera.it/claudiomattera/drone-gotify/releases/tag/1.0.0
